import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

import DisplayTable from './Components/DisplayTable'


class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      isDrawerOpen : false,
      selectedMenu : ''
    }
    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.handleMenuChange = this.handleMenuChange.bind(this)
    this.buildTableUI = this.buildTableUI.bind(this)
  }

  handleTouchTap(){
    this.setState({
      isDrawerOpen : !this.state.isDrawerOpen
    })
  }

  handleMenuChange(e, value) {
    this.setState({
      selectedMenu : value
    })
  }

  buildTableUI(){
    const { selectedMenu } = this.state
    if(selectedMenu.length > 0) {
      return(
        <DisplayTable selection={ selectedMenu }/>
      )
    }
  }

  render() {
    const { isDrawerOpen } = this.state
    return (
      <MuiThemeProvider>
        <div>
          <AppBar
            title="Surefire Local"
            onLeftIconButtonTouchTap={ this.handleTouchTap }
          />

          <Drawer
            docked={false}
            open={ isDrawerOpen }
            onRequestChange={ this.handleTouchTap }
          >
            <Menu onChange={ this.handleMenuChange } >
              <MenuItem value='posts' onClick={ this.handleTouchTap }>Posts</MenuItem>
              <MenuItem value='comments' onClick={ this.handleTouchTap }>Comments</MenuItem>
              <MenuItem value='albums' onClick={ this.handleTouchTap }>Albums</MenuItem>
              <MenuItem value='photos' onClick={ this.handleTouchTap }>Photos</MenuItem>
              <MenuItem value='todos' onClick={ this.handleTouchTap }>Todos</MenuItem>
              <MenuItem value='users' onClick={ this.handleTouchTap }>Users</MenuItem>
            </Menu>
          </Drawer>
          <div id="display-table">
            { this.buildTableUI() }
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
