import React, { Component } from 'react';
import axios from 'axios'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { CardTitle } from 'material-ui/Card';
import Pagination from 'material-ui-pagination'

export default class DisplayTable extends Component {
  constructor(props){
    super(props)

    this.state = {
      selection : props.selection,
      list : [],

      page : 1,
      interval : 10
    }
    this.buildHeaders = this.buildHeaders.bind(this)
    this.buildBody = this.buildBody.bind(this)
    this.getList = this.getList.bind(this)
    this.buildPagination = this.buildPagination.bind(this)
    this.handlePageChange = this.handlePageChange.bind(this)
  }

  componentDidMount() {
    this.getList(this.state.selection)
  }

  componentWillReceiveProps(nextProps){
    const { selection } = nextProps
    if(this.state.selection !== selection) {
      this.getList(selection)
    }
  }

  getList(selection) {
    axios.get('https://jsonplaceholder.typicode.com/' + selection)
    .then((response) => {
      this.setState({
        selection : selection,
        list : response.data,
        page : 1
      })
    })
    .catch((error) => {
      console.log(error)
    })
  }

  buildPagination(){
    const { list, page, interval } = this.state
    let totalPages = (list.length) / interval
    return(
      <Pagination
        total = { totalPages }
        display = { 10 }
        current = { page }
        onChange = { number => this.handlePageChange(number) }
      />
    )
  }

  buildHeaders(){
    const { selection } = this.state
    let headerArray = []
    switch (selection) {
      case 'posts':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>User ID</TableHeaderColumn>,
          <TableHeaderColumn key='4'>Title</TableHeaderColumn>,
          <TableHeaderColumn key='5'>Body</TableHeaderColumn>
        ]
        break;

      case 'comments':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>Post ID</TableHeaderColumn>,
          <TableHeaderColumn key='4'>Name</TableHeaderColumn>,
          <TableHeaderColumn key='5'>Email</TableHeaderColumn>,
          <TableHeaderColumn key='5'>Body</TableHeaderColumn>
        ]
        break;

      case 'albums':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>User ID</TableHeaderColumn>,
          <TableHeaderColumn key='3'>Title</TableHeaderColumn>
        ]
        break;

      case 'photos':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>Album ID</TableHeaderColumn>,
          <TableHeaderColumn key='3'>Title</TableHeaderColumn>,
          <TableHeaderColumn key='4'>Photo</TableHeaderColumn>
        ]
        break;

      case 'todos':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>User ID</TableHeaderColumn>,
          <TableHeaderColumn key='3'>Title</TableHeaderColumn>,
          <TableHeaderColumn key='4'>Completed</TableHeaderColumn>
        ]
        break;

      case 'users':
        headerArray = [
          <TableHeaderColumn key='1'>ID</TableHeaderColumn>,
          <TableHeaderColumn key='2'>Username</TableHeaderColumn>,
          <TableHeaderColumn key='2'>Name</TableHeaderColumn>,
          <TableHeaderColumn key='3'>Email</TableHeaderColumn>,
          <TableHeaderColumn key='4'>Phone</TableHeaderColumn>
        ]
        break;
      default:
    }

    return(
      <TableRow>
        { headerArray }
      </TableRow>
    )
  }

  buildBody() {
    const { selection, list, page, interval } = this.state
    let ui = [], miniList = []

    let lastPage = interval * page
    let firstPage = lastPage - interval

    miniList = list.slice(firstPage, lastPage)

    switch (selection) {
      case 'posts':
        miniList.forEach((value, index) => {
          ui.push(
            <TableRow key={ index }>
              <TableRowColumn>{ value.id }</TableRowColumn>
              <TableRowColumn>{ value.userId }</TableRowColumn>
              <TableRowColumn>{ value.title }</TableRowColumn>
              <TableRowColumn>{ value.body }</TableRowColumn>
            </TableRow>
          )
        })
        break;

      case 'comments':
        miniList.forEach((value, index) => {
          ui.push(
            <TableRow key={ index }>
              <TableRowColumn>{ value.id }</TableRowColumn>
              <TableRowColumn>{ value.postId }</TableRowColumn>
              <TableRowColumn>{ value.name }</TableRowColumn>
              <TableRowColumn>{ value.email }</TableRowColumn>
              <TableRowColumn>{ value.body }</TableRowColumn>
            </TableRow>
          )
        })
        break;

      case 'albums':
        miniList.forEach((value, index) => {
          ui.push(
            <TableRow key={ index }>
              <TableRowColumn>{ value.id }</TableRowColumn>
              <TableRowColumn>{ value.userId }</TableRowColumn>
              <TableRowColumn>{ value.title }</TableRowColumn>
            </TableRow>
          )
        })
        break;

        case 'photos':
          miniList.forEach((value, index) => {
            ui.push(
              <TableRow key={ index }>
                <TableRowColumn>{ value.id }</TableRowColumn>
                <TableRowColumn>{ value.albumId }</TableRowColumn>
                <TableRowColumn>{ value.title }</TableRowColumn>
                <TableRowColumn><img alt={ value.title } src={ value.thumbnailUrl } /></TableRowColumn>
              </TableRow>
            )
          })
          break;

      case 'todos':
        miniList.forEach((value, index) => {
          ui.push(
            <TableRow key={ index }>
              <TableRowColumn>{ value.id }</TableRowColumn>
              <TableRowColumn>{ value.userId }</TableRowColumn>
              <TableRowColumn>{ value.title }</TableRowColumn>
              <TableRowColumn>{ (value.completed) ? "TRUE" : "FALSE" }</TableRowColumn>
            </TableRow>
          )
        })
        break;

      case 'users':
        miniList.forEach((value, index) => {
          ui.push(
            <TableRow key={ index }>
              <TableRowColumn>{ value.id }</TableRowColumn>
              <TableRowColumn>{ value.username }</TableRowColumn>
              <TableRowColumn>{ value.name }</TableRowColumn>
              <TableRowColumn>{ value.email }</TableRowColumn>
              <TableRowColumn>{ value.phone }</TableRowColumn>
            </TableRow>
          )
        })
        break;
      default:
    }


    return ui
  }

  handlePageChange(number) {

    this.setState({
      page : number
    })
  }

  render(){
    const { selection } = this.state
    return(
      <Paper zDepth={1}>
        <CardTitle title={ selection.toUpperCase() } />
        { this.buildPagination() }
        <Table>
          <TableHeader>
            { this.buildHeaders() }
          </TableHeader>
          <TableBody>
            { this.buildBody() }
          </TableBody>
        </Table>
      </Paper>
    )
  }
}
