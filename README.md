## Surefire local App
A ReactJS app with AJAX functionalities. Designed using Material UI.

## To install App

Go to root directory and run:
### npm install

## To run App
Go to root directory and run:
### npm start
